
from odoo import models, fields, api


class Buku(models.Model):
    _name = 'koleksi.buku'
    _description = 'Koleksi Buku'
    _sql_constraints = [
        ('no_isbn', 'UNIQUE(isbn)', 'No. ISBN Harus Unik')
    ]

    name = fields.Char(string='Judul')
    category = fields.Selection([('general', 'Umum'), ('technology', 'IT'), ('health', 'Kesehatan'), ('politics', 'Politik')], string='Kategori', help='Jenis Kategori')
    release_date = fields.Date(string='Tanggal Rilis')
    author = fields.Many2many('res.partner', string='Penulis')
    isbn = fields.Text(string='Kode ISBN')
    books_ids = fields.Many2one('member.transaction', string='Judul Buku')
    books_title = fields.Many2one('koleksi.buku', string='Judul Buku')


class Member(models.Model):
    _name = 'member.buku'
    _description = 'Daftar Member'
    _sql_constraints = [
        ('no_identity', 'UNIQUE(identity)', 'No. Identitas Harus Unik')
    ]

    name = fields.Char(string='Nama Member', required=True)
    identity = fields.Char(string='No. Identitas', required=True)
    type_identity = fields.Selection([('ktp', 'KTP'), ('sim', 'SIM'), ('passport', 'Paspor')], string='Kartu Identitas', required=True, help='Jenis Kartu Identitas')
    state = fields.Selection([('draft', 'Draf'), ('approved', 'Disetujui')], string='Status', required=True, help='Status')
    


class RentalTransaction(models.Model):
    _name = 'member.transaction'
    _description = 'Transaksi Rental'

    @api.depends('borrowed_date', 'return_date')
    def compute_duration(self):
        for day in self:
            day.borrow_duration = 0
            if day.borrowed_date and day.return_date:
                day.borrow_duration = day.return_date - day.borrowed_date

    
    @api.model
    def create(self, vals):
        vals['transaction_number'] = self.env['ir.sequence'].next_by_code('member.transaction')
        return super(RentalTransaction, self).create(vals)
        
    transaction_number = fields.Char(string='No. Transaksi', readonly=True, default='/')
    transaction_date = fields.Date(string='Tanggal Rental')
    name_borrower = fields.Many2one('member.buku', string='Nama Peminjam')
    session_line = fields.One2many('koleksi.buku', 'books_ids', string='Koneksi')
    borrowed_date = fields.Date(string='Tanggal Pinjam')
    return_date = fields.Date(string='Tanggal Kembali')
    borrow_duration = fields.Char(string='Durasi Peminjaman (Hari)', compute=compute_duration, readonly=True)
    price = fields.Integer(string='Total Biaya Sewa')

    